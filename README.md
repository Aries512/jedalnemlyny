# Jedálne Mlyny #

Simple Android app showing current menu in canteens in *Mlynská dolina* university campus.

Data is obtained by ugly PHP script parsing website with daily menus. Script should be run on a server. Resulting XML containing all parsed data is then downloaded by the client app and displayed in irresistibly fancy UI.