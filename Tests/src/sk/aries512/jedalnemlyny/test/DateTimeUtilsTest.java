package sk.aries512.jedalnemlyny.test;

import java.util.Calendar;
import org.junit.Test;
import sk.aries512.jedalnemlyny.utils.DateTimeUtils;
import android.test.AndroidTestCase;

public class DateTimeUtilsTest extends AndroidTestCase {


	@Test
	public void testAreSameDay() {

		Calendar now = Calendar.getInstance();
		assertTrue(DateTimeUtils.areSameDay(now, now));

		Calendar a = Calendar.getInstance();
		a.set(2014, 6, 15, 15, 15, 15);
		Calendar b = Calendar.getInstance();
		b.set(2014, 6, 15, 0, 20, 0);
		assertTrue(DateTimeUtils.areSameDay(a, b));

		b.set(2014, 6, 16, 0, 20, 0);
		assertFalse(DateTimeUtils.areSameDay(a, b));

		a.set(Calendar.DAY_OF_MONTH, 16);
		assertTrue(DateTimeUtils.areSameDay(a, b));

	}



	@Test
	public void testGetDayToShow() {

		Calendar a = Calendar.getInstance();
		a.set(2014, 6, 15, 0, 15, 15);
		assertTrue(DateTimeUtils.areSameDay(a, DateTimeUtils.getDayToShow(a, 20)));

		a.set(Calendar.HOUR_OF_DAY, 15);
		assertTrue(DateTimeUtils.areSameDay(a, DateTimeUtils.getDayToShow(a, 20)));

		a.set(Calendar.HOUR_OF_DAY, 21);
		assertTrue(DateTimeUtils.areSameDay(a, DateTimeUtils.getDayToShow(a, 22)));

		Calendar shown = DateTimeUtils.getDayToShow(a, 20);
		assertFalse(DateTimeUtils.areSameDay(a, shown));
		a.add(Calendar.DAY_OF_MONTH, 1);
		assertTrue(DateTimeUtils.areSameDay(a, shown));

	}

}
