package sk.aries512.jedalnemlyny.test;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.test.AndroidTestCase;

public class PreferencesUtilsTest extends AndroidTestCase {


	@Test
	public void testSaveString() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_string1").remove("prefkey_test_string2").commit();

		PreferencesUtils.saveString(getContext(), "prefkey_test_string1", "test1");
		assertEquals("test1", sp.getString("prefkey_test_string1", ""));

		PreferencesUtils.saveString(getContext(), "prefkey_test_string1", "test2");
		assertEquals("test2", sp.getString("prefkey_test_string1", ""));

		PreferencesUtils.saveString(getContext(), "prefkey_test_string2", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|");
		assertEquals("TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", sp.getString("prefkey_test_string2", ""));
		assertEquals("test2", sp.getString("prefkey_test_string1", ""));

	}

	@Test
	public void testLoadString() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_string1").remove("prefkey_test_string2").commit();

		sp.edit().putString("prefkey_test_string1", "test1").commit();
		assertEquals("test1", PreferencesUtils.loadString(getContext(), "prefkey_test_string1", ""));

		sp.edit().putString("prefkey_test_string1", "test2").commit();
		assertEquals("test2", PreferencesUtils.loadString(getContext(), "prefkey_test_string1", ""));

		sp.edit().putString("prefkey_test_string2", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|").commit();
		assertEquals("TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", PreferencesUtils.loadString(getContext(), "prefkey_test_string2", ""));
		assertEquals("test2", PreferencesUtils.loadString(getContext(), "prefkey_test_string1", ""));

	}



	@Test
	public void testSaveInt() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_int1").remove("prefkey_test_int2").commit();

		PreferencesUtils.saveInt(getContext(), "prefkey_test_int1", 42);
		assertEquals(42, sp.getInt("prefkey_test_int1", 0));

		PreferencesUtils.saveInt(getContext(), "prefkey_test_int1", Integer.MAX_VALUE);
		assertEquals(Integer.MAX_VALUE, sp.getInt("prefkey_test_int1", 0));

		PreferencesUtils.saveInt(getContext(), "prefkey_test_int2", Integer.MIN_VALUE);
		assertEquals(Integer.MIN_VALUE, sp.getInt("prefkey_test_int2", 0));
		assertEquals(Integer.MAX_VALUE, sp.getInt("prefkey_test_int1", 0));

	}

	@Test
	public void testLoadInt() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_int1").remove("prefkey_test_int2").commit();

		sp.edit().putInt("prefkey_test_int1", 42).commit();
		assertEquals(42, PreferencesUtils.loadInt(getContext(), "prefkey_test_int1", 0));

		sp.edit().putInt("prefkey_test_int1", Integer.MAX_VALUE).commit();
		assertEquals(Integer.MAX_VALUE, PreferencesUtils.loadInt(getContext(), "prefkey_test_int1", 0));

		sp.edit().putInt("prefkey_test_int2", Integer.MIN_VALUE).commit();
		assertEquals(Integer.MIN_VALUE, PreferencesUtils.loadInt(getContext(), "prefkey_test_int2", 0));
		assertEquals(Integer.MAX_VALUE, PreferencesUtils.loadInt(getContext(), "prefkey_test_int1", 0));

	}



	@Test
	public void testSaveLoadStringList() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_stringlist1").remove("prefkey_test_stringlist2").commit();

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1");
		assertEquals(new ArrayList<String>(), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a");
		assertEquals(Arrays.asList("a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a", "b");
		assertEquals(Arrays.asList("a", "b"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a", "a");
		assertEquals(Arrays.asList("a", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a", "b", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "a");
		assertEquals(Arrays.asList("a", "b", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist2", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|");
		assertEquals(Arrays.asList("TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist2"));
		assertEquals(Arrays.asList("a", "b", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

	}



	@Test
	public void testAddNewUniqueToStringList() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_stringlist1").remove("prefkey_test_stringlist2").commit();

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1");
		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1");
		assertEquals(new ArrayList<String>(), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1", "a");
		assertEquals(Arrays.asList("a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1", "b");
		assertEquals(Arrays.asList("a", "b"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1", "a");
		assertEquals(Arrays.asList("a", "b"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1");
		assertEquals(Arrays.asList("a", "b"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist1", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "a", "A");
		assertEquals(Arrays.asList("a", "b", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "A"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist2");
		PreferencesUtils.addNewUniqueToStringList(getContext(), "prefkey_test_stringlist2", "fffff", "ggggggg");
		assertEquals(Arrays.asList("fffff", "ggggggg"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist2"));
		assertEquals(Arrays.asList("a", "b", "TesT~*-+!@#$%^&()[]{}';\\/.,<>?:\"|", "", "A"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

	}



	@Test
	public void testRemoveFromStringList() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		sp.edit().remove("prefkey_test_stringlist1").remove("prefkey_test_stringlist2").commit();

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a", "b", "c", "d", "c", "", "a");
		assertEquals(Arrays.asList("a", "b", "c", "d", "c", "", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist1");
		assertEquals(Arrays.asList("a", "b", "c", "d", "c", "", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist1", "b");
		assertEquals(Arrays.asList("a", "c", "d", "c", "", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist1", "a");
		assertEquals(Arrays.asList("c", "d", "c", ""), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist1", "", "c");
		assertEquals(Arrays.asList("d"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist1", "", "d", "x", "a", "x", "d");
		assertEquals(Arrays.asList(), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));

		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist1", "a", "b", "a");
		PreferencesUtils.saveStringList(getContext(), "prefkey_test_stringlist2", "a", "b", "a");
		PreferencesUtils.removeFromStringList(getContext(), "prefkey_test_stringlist2", "", "a", "x", "a");
		assertEquals(Arrays.asList("b"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist2"));
		assertEquals(Arrays.asList("a", "b", "a"), PreferencesUtils.loadStringList(getContext(), "prefkey_test_stringlist1"));
	}






}
