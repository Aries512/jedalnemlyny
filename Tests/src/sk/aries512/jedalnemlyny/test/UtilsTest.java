package sk.aries512.jedalnemlyny.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.test.AndroidTestCase;

public class UtilsTest extends AndroidTestCase {

	@Test
	public void testContainsIgnoreCase() {

		assertFalse(Utils.containsIgnoreCase(new ArrayList<String>(), null));
		assertFalse(Utils.containsIgnoreCase(new ArrayList<String>(), ""));
		assertFalse(Utils.containsIgnoreCase(new ArrayList<String>(), "a"));
		assertFalse(Utils.containsIgnoreCase(new ArrayList<String>(), "A"));

		List<String> list = new ArrayList<String>();
		list.add(null);
		assertTrue(Utils.containsIgnoreCase(list, null));
		assertFalse(Utils.containsIgnoreCase(list, ""));
		assertFalse(Utils.containsIgnoreCase(list, "a"));
		assertFalse(Utils.containsIgnoreCase(list, "A"));

		assertFalse(Utils.containsIgnoreCase(Arrays.asList(""), null));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList(""), ""));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList(""), "a"));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList(""), "A"));

		assertFalse(Utils.containsIgnoreCase(Arrays.asList("a"), null));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("a"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("a"), "a"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("a"), "A"));

		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A"), null));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A"), "a"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A"), "A"));

		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", "a"), null));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", "a"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", "a"), "a"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", "a"), "A"));

		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a"), null));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", null, "a"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a"), "a"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a"), "A"));

		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), null));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), ""));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), " "));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "a"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "A"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "b"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "B"));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "c"));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("A", null, "a", "b", ""), "C"));

		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), null));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), " "));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "aa"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "AA"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "aA"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "Aa"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "cd"));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "c"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "a b c"));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, "aA", "a", "b", "", "cD", " ", "a B c"), "A B C"));

		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), null));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), ""));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), " "));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), "A B C"));
		assertFalse(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), "A B C "));
		assertTrue(Utils.containsIgnoreCase(Arrays.asList("Aa", null, " ", "a B c", " ( / h"), " ( / H"));

	}

}
