package sk.aries512.jedalnemlyny.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.junit.Test;
import sk.aries512.jedalnemlyny.model.Canteen;
import sk.aries512.jedalnemlyny.model.DailyMenu;
import sk.aries512.jedalnemlyny.model.MenuCategory;
import sk.aries512.jedalnemlyny.model.MenuItem;
import sk.aries512.jedalnemlyny.model.MetaData;
import sk.aries512.jedalnemlyny.model.XmlParser;
import sk.aries512.jedalnemlyny.model.XmlParser.XmlParserException;
import android.test.AndroidTestCase;

public class XmlParserTest extends AndroidTestCase {



	private XmlParser prepareParser(int resourceId) throws XmlParserException {
		InputStream is = getContext().getResources().openRawResource(resourceId);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int i;
		try {
			i = is.read();
			while (i != -1)
			{
				byteArrayOutputStream.write(i);
				i = is.read();
			}
			is.close();
		} catch (IOException e) { }
		XmlParser parser = new XmlParser(byteArrayOutputStream.toString());
		return parser;
	}



	@Test
	public void testMetaDataTimeout() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		assertEquals(1800, metaData.timeoutSeconds);
	}



	@Test
	public void testMetaDataInfo() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		assertEquals("some info", metaData.info);
	}



	@Test
	public void testMetaDataDate() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		Calendar cal = Calendar.getInstance();
		cal.set(2014, 2, 29, 11, 15, 4);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
		assertEquals(format.format(cal.getTime()), format.format(metaData.creationDateTime.getTime()));
	}



	@Test
	public void testMetaDataColumnNames() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		assertEquals(5, metaData.columnNames.size());
		assertEquals("Názov", metaData.columnNames.get(0).full);
		assertEquals("Názov", metaData.columnNames.get(0).shortt);
		assertEquals("Kvantita", metaData.columnNames.get(1).full);
		assertEquals("Kvant.", metaData.columnNames.get(1).shortt);
		assertEquals("Alergény", metaData.columnNames.get(2).full);
		assertEquals("Alerg.", metaData.columnNames.get(2).shortt);
		assertEquals("Cena", metaData.columnNames.get(3).full);
		assertEquals("", metaData.columnNames.get(3).shortt);
		assertEquals("Cena s dotáciou", metaData.columnNames.get(4).full);
		assertEquals("", metaData.columnNames.get(4).shortt);
	}



	@Test
	public void testMetaDataErrors() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		assertEquals(2, metaData.usedOld.size());
		assertTrue(metaData.usedOld.contains("name1"));
		assertTrue(metaData.usedOld.contains("name2"));
		assertEquals(2, metaData.notLoaded.size());
		assertTrue(metaData.notLoaded.contains("Venza"));
		assertTrue(metaData.notLoaded.contains("Eat and Meet"));
	}



	@Test
	public void testMetaServers() throws XmlParserException {
		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		MetaData metaData = parser.getMetaData();
		assertEquals(2, metaData.serverLocations.size());
		assertTrue(metaData.serverLocations.get(0).compressed.equals("davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml.gz"));
		assertTrue(metaData.serverLocations.get(0).uncompressed.equals("davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml"));
		assertTrue(metaData.serverLocations.get(1).compressed.equals("jedalne.host-ed.me/jedalne/jedalne.xml.gz"));
		assertTrue(metaData.serverLocations.get(1).uncompressed.equals("jedalne.host-ed.me/jedalne/jedalne.xml"));
	}



	@Test
	public void testCanteen() throws XmlParserException {

		XmlParser parser = prepareParser(sk.aries512.jedalnemlyny.R.raw.testxml1);
		List<Canteen> canteens = parser.getCanteenData();
		assertEquals(2, canteens.size());

		Canteen cant = canteens.get(0);
		assertEquals("Venza", cant.name);
		assertEquals("http://www.mlynska-dolina.sk/stravovanie/vsetky-zariadenia/venza/denne-menu", cant.source);
		Calendar cal = Calendar.getInstance();
		cal.set(2014, 2, 29, 11, 15, 2);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
		assertEquals(format.format(cal.getTime()), format.format(cant.lastUpdate.getTime()));

		assertEquals(2, cant.dailyMenus.size());
		DailyMenu menu = cant.dailyMenus.get(0);
		cal = Calendar.getInstance();
		cal.set(2014, 3, 29);
		format = new SimpleDateFormat("yyyy-MM-dd");
		//assertEquals(format.format(cal.getTime()), format.format(menu.day.getTime()));

		assertEquals(2, menu.categories.size());
		MenuCategory cat = menu.categories.get(0);
		assertEquals("Polievky", cat.name);

		assertEquals(2, cat.items.size());
		MenuItem item = cat.items.get(0);
		assertEquals("pol", item.name);
		assertEquals("1,7", item.allergenes);
		assertEquals("0,33l", item.quantity);
		assertEquals("0.4", item.priceStudent);
		assertEquals("0.4", item.priceFull);


	}

}
