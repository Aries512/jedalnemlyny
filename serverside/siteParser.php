<?php

	// I can't code in PHP
	// This is my first script
	// Hopefully the last too
	// It is ugly and full of hacks
	// But not as much as PHP itself
	// I don't want to code in PHP
	// It hurts
	
	
	
	$names = array("Eat & Meet", "Venza");
	$systemNames = array("eatandmeet", "venza");
	
	$urls = array(
		"http://www.mlynska-dolina.sk/stravovanie/vsetky-zariadenia/eat-meet/denne-menu",
		"http://www.mlynska-dolina.sk/stravovanie/vsetky-zariadenia/venza/denne-menu",
		);
	
	$servers = array(
		array("http://davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml", "http://davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml.gz"),
		array("http://jedalne.host-ed.me/jedalne/jedalne.xml", "http://jedalne.host-ed.me/jedalne/jedalne.xml.gz"),
	);
	
	$timeout = 1200;
	
	$info = "";
	
	$columnNames = array(
		array("Názov", "Názov"),
		array("Kvantita", "Kvant."),
		array("Alergény", "Alerg."),
		array("Cena", ""),
		array("Cena s dotáciou", ""),
	);
	
	//replacements for parsed table headers, to achieve uniformity, order and light in our lives
	$categoryNamesReplacement = array(
		"hlavné" => "Hlavné jedlá",
		"polievk" => "Polievky",
		"prílohy" => "Prílohy",
		"tovar prílohový" => "Prílohy",
		"iné" => "Prílohy",
		"dresing" => "Prílohy",
		//two version of this word, because we can't lowercase national character (see strtolowerAlpha function comment)
		"šalát" => "Šaláty",
		"Šalát" => "Šaláty",
		"nápoj" => "Nápoje",
		"studen" => "Studené jedlá",
		"doplnk" => "Studené jedlá",
		"raňajk" => "Raňajky",
		"nadštandar" => "Nadštandardné jedlá"
	);
	
	//order of categories defined by their replaced names
	$categoryOrder = array(
		"Hlavné jedlá",
		"Nadštandardné jedlá",
		"Polievky",
		"Šaláty",
		"Prílohy",
		"Nápoje",
		"Raňajky",
		"Studené jedlá",
	);
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
	date_default_timezone_set("Europe/Bratislava");
	
	set_error_handler("onError", E_ALL);
	set_exception_handler("onException");
	
	include('simple_html_dom.php');
	include('xmlGenerator.php');
	
	$xmls = array();
	
	for ($siteIdx = 0; $siteIdx < count($urls); $siteIdx++) {
		
		$html = file_get_html($urls[$siteIdx]);
		if ($html == null) {
			trigger_error($systemNames[$siteIdx] . " not parsed, server did not respond.");
			continue;
		}
		
		//find tables after menu header
		$start = findElementWithText($html, "h2", "menu");
		if ($start == null || $start->nextSibling() == null) {
			trigger_error($systemNames[$siteIdx] . " not parsed, parsing error.");
			continue;
		}
		$tables = $start->nextSibling()->find("table");
		
		$canteenData = new CanteenData($names[$siteIdx], $systemNames[$siteIdx], $urls[$siteIdx], date("Y-m-d,H:i:s"));
		$tableDate = null;
		$hasParsedAnyTable = false;
		
		foreach ($tables as $table) {
		
			$tableDateTmp = getTableDate($table);
			if ($tableDateTmp != null) {
				$tableDate = $tableDateTmp;
			}
			
			$tableCategory = getTableCategory($table);
			if ($tableCategory == null || $tableDate == null) {
				trigger_error($systemNames[$siteIdx] . " table not parsed, headline or date missing.");
				continue;
			}
			$tableCategory = getTableCategoryName($tableCategory, $categoryNamesReplacement);
			
			$content = getTableContent($table);
			if ($content == null || count($content) == 0) {
				trigger_error($systemNames[$siteIdx] . " table not parsed, content missing.");
				continue;
			}
			
			$hasParsedAnyTable = true;
			$canteenData->addDayMenu($content, $tableDate, $tableCategory);
		}
		
		if ($hasParsedAnyTable) {
			$canteenData->orderCategories($categoryOrder);
			$xml = $canteenData->saveXml();
			$xmls[$names[$siteIdx]] = $xml;
		}
			
	}
	
	saveFinalXml($xmls, $names, $systemNames, $servers, $timeout, $info, $columnNames);
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
	function findElementWithText($html, $element, $text) {
		foreach ($html->find($element) as $e) {
			if (strpos($e->innertext, $text) !== false) {
				return $e;
			}
		}
		return null;
	}
	
	
	function getTableCategory($table) {
		if ($table->previousSibling() != null && $table->previousSibling()->tag == "p") {
			return $table->previousSibling()->plaintext;
		}
		return null;
	}
	
	
	function getTableCategoryName($str, $categoryNamesReplacement) {
		foreach ($categoryNamesReplacement as $key => $value) {
			if (strpos(strtolowerAlpha($str), strtolowerAlpha($key)) !== false) {
				return $value;
			}
		}
		return $str;
	}
	
	
	function getTableDate($table) {
		if ($table->previousSibling() == null || $table->previousSibling()->previousSibling() == null) {
			return null;
		}
		$ppElem = $table->previousSibling()->previousSibling();
		if ($ppElem->tag == "h2" && count($ppElem->children)>0 && $ppElem->children[0]->tag == "a" && $ppElem->children[0]->hasAttribute("name")) {
			return $ppElem->children[0]->name;
		}
		return null;
	}
	
	
	
	function getTableContent($table) {
		$cells = $table->find("td");
		if (count($cells)%2==1) {
			trigger_error("Odd number of table cells.");
			return null;
		}
		
		$content = array();
		
		for ($i=0; $i<count($cells); $i=$i+2) {
			$cell1 = html_entity_decode($cells[$i]->plaintext);
			$cell2 = html_entity_decode($cells[$i+1]->plaintext);
			$cell1 = removeHardSpaces($cell1);
			$cell2 = removeHardSpaces($cell2);
			$items = getItemsFromCell($cell1);
			$prices = getPricesFromCell($cell2);
			if ($items == null) {
				continue;
			}
			$cellContent = array_merge($items, $prices);
			array_push($content, $cellContent);
		}
		
		return $content;
	}
	
	
	function getPricesFromCell($cell) {
		$cell = explode("/", $cell);
		if (count($cell) != 2) {
			trigger_error("Incorrect format of price cell.");
			return array("priceFull" => "-", "price" => "-");
		}
		//remove all that is not a number or a dot
		$cell[0] = preg_replace("#[^\d\.]#","",$cell[0]);
		$cell[1] = preg_replace("#[^\d\.]#","",$cell[1]);
		return array("priceFull" => $cell[0], "price" => $cell[1]);
	}
	
	
	//returns null if cell is not correctly formated
	function getItemsFromCell($cell) {
	//beware, brave programmer! ugly code ahead!
	//only those with no fear in heart may enter
		
		$cell = trim($cell);
		
		//remove unnecessary standard-breaking trailing ketchup and mustard
		//that was my most stupid comment ever
		$cell = removeTrailingStringAndTrim($cell, "kečup");
		$cell = removeTrailingStringAndTrim($cell, "horčica");
		
		if (strlen($cell) < 3) return null;
		
		//allergens are placed at the end in the format ".../num1,num2.../" or "...(num1,num2...)"
		$allerg = "";
		$lastChar = $cell[strlen($cell)-1];
		$lastChar1 = $cell[strlen($cell)-2];
		$lastChar2 = $cell[strlen($cell)-3];
		if ($lastChar == ')' || $lastChar == '/' || (is_numeric($lastChar) &&
		(!is_numeric($lastChar1) || ($lastChar1 == '1' && !is_numeric($lastChar2))))) {
			$cell = substr($cell, 0, strlen($cell)-1);
			$idxPar = strrpos($cell, '(');
			$idxSlash = strrpos(substr($cell, 0, strlen($cell)-1), '/');
			$idx = 0;
			if ($lastChar == ')') {
				$idx = $idxPar;
			} else if ($lastChar == '/') {
				$idx = $idxSlash;
			} else if (is_numeric($lastChar)) {
				// there are some items with closing parenthesis missing in format "something 42g (1,3,7", this is an attempt to solve it
				$idx = max($idxPar, $idxSlash);
			}
			if ($idx === false || $idx === 0) {
				trigger_error("Incorrect format of allergens.");
				//we do not expect this, better return null
				return null;
			}
			$allerg = substr($cell, $idx+1, strlen($cell) - $idx - (is_numeric($lastChar) ? 0 : 1));
			//remove everything that is not a number or a comma (there are typos on a site sometimes)
			$allerg = preg_replace("#[^\d\,]#", "", $allerg);
			if (!strcmp($allerg, "0")) $allerg = "";
			//cut off alegenes from the rest
			$cell = substr($cell, 0, $idx);
			$cell = trim($cell, " \t\n\r\0\x0B\,");
			$cell = removeTrailingStringAndTrim($cell, "kečup");
			$cell = removeTrailingStringAndTrim($cell, "horčica");
		}
		
		//weight/volume, formatted as ...num ml/dcl/l/g
		$quantityStart = -1;
		$removeTypos = true;
		if (endsWith(strtolower($cell), "ml")) {
			$quantityStart = getTrailingQuantityPosition(substr($cell, 0, strlen($cell)-2));
		} else if (endsWith(strtolower($cell), "dcl")) {
			$quantityStart = getTrailingQuantityPosition(substr($cell, 0, strlen($cell)-3));
		} else if (strtolower($cell[strlen($cell)-1]) == 'g') {
			$quantityStart = getTrailingQuantityPosition(substr($cell, 0, strlen($cell)-1));
		} else if (strtolower($cell[strlen($cell)-1]) == 'l') {
			$quantityStart = getTrailingQuantityPosition(substr($cell, 0, strlen($cell)-1));
			$removeTypos = false;
		} else if (is_numeric($cell[strlen($cell)-1])) {
			$quantityStart = getTrailingQuantityPosition(substr($cell, 0, strlen($cell)));
		}
		
		$quantity = "";
		if ($quantityStart >= 0) {
			$quantity = str_replace(" ", "", substr($cell, $quantityStart));
		
			//common typos on the site, remove starting "0," or ","
			if ($removeTypos) {
				if (strpos($quantity, "0,") === 0) {
					$quantity = substr($quantity, 2);
				}
				if ($quantity[0] == ',') {
					$quantity = substr($quantity, 1);
				}
			}
			//cut off quantity from the rest
			$cell = trim(substr($cell, 0, $quantityStart), " \t\n\r\0\x0B\,");
		}
		
		// fix ugly commas and dots without spaces after them (learn typography you barbarians!)
		$cell = preg_replace("#,(?!\s)#", ", ", $cell);
		$cell = preg_replace("#\.(?!\s)#", ". ", $cell);
		$cell = trim($cell);
		
		// some stray "Menu" string in the end of the name... no idea why, but we don't want it there
		if (endsWith(strtolowerAlpha($cell), "menu")) {
			$cell = substr($cell, 0, $cell - 4);
			$cell = trim($cell);
		}
		
		return array("name"=>$cell, "quantity"=>$quantity, "allergens"=>$allerg);
	}
	
	
	//returns starting position of trailing quantity string (must contain only numbers, comma, dot or slash)
	//return -1 if trailing quantity string is not present or is not correct
	function getTrailingQuantityPosition($str) {
		$str = trim($str);
		if (is_numeric($str[strlen($str)-1])) {
			for ($i = strlen($str)-2; $i>=0 && strpos('0123456789 ,./', $str[$i])!==false; $i--);
			for ($j = $i+1; $j<strlen($str) && strpos('0123456789', $str[$j])===false; $j++);
			return $j;
		}
		return -1;
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
	function isLastCharEquals($str, $char) {
		return $str[strlen($str)-1] == $char;
	}
	
	
	function removeTrailingStringAndTrim($str, $trailing) {
		if (endsWith($str, $trailing)) {
			$str = trim(substr($str, 0, strlen($str)-strlen($trailing)), " \t\n\r\0\x0B\,\.");
		}
		return $str;
	}
	
	
	function endsWith($haystack, $needle) {
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
		return (substr($haystack, -$length) === $needle);
	}

	
	function removeHardSpaces($str) {
		$str = str_replace("#[\xA0]#", "", $str);
		$str = str_replace("#[\xC2]#", "", $str);
		return $str;
	}
	
	
	//yeah, we have 1990 or so and PHP doesn't support national characters by default.
	//and as on freehosting I have no access to php.ini, I can't enable mbstring.
	//thus we need this stupid and barely working hack converting only A-Z.
	//thank you PHP!
	function strtolowerAlpha($str) {
		for ($i = 0; $i < strlen($str); $i++) {
			if ($str[$i] >= 'A' && $str[$i] <= 'Z') {
				$str[$i] = chr(ord($str[$i]) + 32);
			}
		}
		return $str;
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
	function onError($errno, $message, $file, $line, $context) {
		logError($errno, $message, $file, $line);
	}
	function onException($exception) {
		logError(get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine());
	}
	
	
	function logError($type, $message, $file, $line) {
		$err = date("Y-m-d,H:i:s | ") . $type . " | " . $file . " | Line " . $line . " | " .$message . "\n";
		echo $err;
		file_put_contents("errlog.txt", $err, FILE_APPEND);
	}
	
?>