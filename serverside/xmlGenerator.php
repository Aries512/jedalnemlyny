<?php

	$dataPath = "xml/";

	function saveFinalXml($xmls, $names, $systemNames, $servers, $timeout, $info, $columnNames) {
	
		global $dataPath;
		
		$dom = new DOMDocument("1.0", "utf-8");
		$dom->substituteEntities = false;
			
		$rootNode = addChild($dom, $dom, "jedalneMlyny");
		$metaNode = addChild($dom, $rootNode, "meta");
		addChild($dom, $metaNode, "datetime", date("Y-m-d,H:i:s"));
		
		$serversNode = $dom->createElement("servers");
		foreach ($servers as $server) {
			$serverNode = addChild($dom, $serversNode, "server");
			addChild($dom, $serverNode, "uncompressed", $server[0]);
			addChild($dom, $serverNode, "compressed", $server[1]);
		}
		$metaNode->appendChild($serversNode);
		
		addChild($dom, $metaNode, "timeout", $timeout);
		
		addChild($dom, $metaNode, "info", $info);
		
		$columnsNode = $dom->createElement("columnNames");
		foreach ($columnNames as $cName) {
			$columnNode = addChild($dom, $columnsNode, "columnName");
			addChild($dom, $columnNode, "full", $cName[0]);
			addChild($dom, $columnNode, "short", $cName[1]);
		}
		$metaNode->appendChild($columnsNode);
		
		$errorsNode = $dom->createElement("errors");
		$notLoadedNode = $dom->createElement("notLoaded");
		$usedOldNode = $dom->createElement("usedOld");
		
		for ($namesIdx = 0; $namesIdx < count($names); $namesIdx++) {
			
			$name = $names[$namesIdx];
			
			if (array_key_exists($name, $xmls)) {
				$xml = $xmls[$name];
				$added = addToFinalXml($xml, $dom, $rootNode);
				if (!$added) {
					addChild($dom, $notLoadedNode, "name", $name);
				}
			
			} else {
				$stored = getStoredXml($dataPath . $systemNames[$namesIdx] . ".xml");
				$added = false;
				if ($stored != null) {
					$added = addToFinalXml($stored, $dom, $rootNode);
				}
				addChild($dom, $added ? $usedOldNode : $notLoadedNode, "name", $name);
			}
		}
		
		$errorsNode->appendChild($notLoadedNode);
		$errorsNode->appendChild($usedOldNode);
		$metaNode->appendChild($errorsNode);
		$dom->formatOutput = true;
		//echo $dom->saveXML();
		$dom->save($dataPath . "jedalne.xml");
	}
	
	
	function addToFinalXml($xmlCanteen, $xmlFinalDom, $xmlFinalDataNode) {
		$canteen = $xmlCanteen->getElementsByTagName("canteen");
		if ($canteen->length != 1) {
			return false;
		} else {
			$xmlFinalDataNode->appendChild($xmlFinalDom->importNode($canteen->item(0), true));
			return true;
		}
	}
	
	
	
	function getStoredXml($filename) {
		if (!file_exists($filename)) {
			return null;
		}
		$dom = new DOMDocument();
		if ($dom->load($filename)) {
			return $dom;
		} else {
			return null;
		}
	}


	function addChild($dom, $node, $name, $value=null, $attr=null, $attrVal=null) {
		$child = $dom->createElement($name);
		if ($value !== null) {
			$child->appendChild($dom->createTextNode($value));
		}
		if ($attr != null && $attrVal != null) {
			$child->setAttribute($attr, $attrVal);
		}
		$node->appendChild($child);
		return $child;
	}
	

	
	class CanteenData {
		
		public $name;
		public $systemName;
		public $dataSource;
		public $dataDate;
		protected $data = array();
		
		function  __construct($name, $systemName, $dataSource, $dataDate) {
			$this->name = $name;
			$this->systemName = $systemName;
			$this->dataSource = $dataSource;
			$this->dataDate = $dataDate;
		}
		
		
		public function addDayMenu($menu, $date, $category) {
			//I know, code smell
			if (!array_key_exists($date, $this->data)) {
				$this->data[$date] = array();
			}
			if (!array_key_exists($category, $this->data[$date])) {
				$this->data[$date][$category] = array();
			}
			$this->data[$date][$category] = array_merge($this->data[$date][$category], $menu);
		}
		
		
		public function orderCategories($order) {
			
			foreach ($this->data as $date => $value) {
				
				$ordered = array();
				
				//check given order of categories and pick categories
				//accordingly from our data
				foreach ($order as $ordCategory) {
					foreach ($value as $category => $items) {
						if ($category == $ordCategory) {
							if (!array_key_exists($category, $ordered)) {
								$ordered[$category] = array();
							}
							$ordered[$category] = array_merge($ordered[$category], $items);
						}
					}
				}
				
				//add the rest of the categories not contained in given order
				foreach ($value as $category => $items) {
					if (!in_array($category, $order)) {
						if (!array_key_exists($category, $ordered)) {
							$ordered[$category] = array();
						}
						$ordered[$category] = array_merge($ordered[$category], $items);
					}
				}
				
				$this->data[$date] = $ordered;
			}
		}
		
		
		public function saveXml() {
		
			global $dataPath;

			$dom = new DOMDocument("1.0", "utf-8");
			$dom->substituteEntities = false;
			
			$rootNode = addChild($dom, $dom, "canteen");
			addChild($dom, $rootNode, "name", $this->name);
			addChild($dom, $rootNode, "lastUpdate", $this->dataDate);
			addChild($dom, $rootNode, "source", $this->dataSource);
			
			foreach ($this->data as $date => $value) {
				$dailyMenuNode = addChild($dom, $rootNode, "dailyMenu", null, "date", $date);
				foreach ($value as $category => $items) {
					$categoryNode = addChild($dom, $dailyMenuNode, "category", null, "name", $category);
					foreach ($items as $item) {
						$itemNode = addChild($dom, $categoryNode, "item");
						foreach ($item as $attr => $value2) {
							addChild($dom, $itemNode, $attr, $value2);
						}
					}
				}
			}
			
			$dom->formatOutput = true;
			$dom->save($dataPath . ($this->systemName) . ".xml");
			return $dom;
		}
		
	}

?>
