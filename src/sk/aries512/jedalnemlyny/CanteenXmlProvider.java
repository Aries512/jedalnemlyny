package sk.aries512.jedalnemlyny;

import android.content.Context;

public interface CanteenXmlProvider {

	public String getXmlContent(Context context) throws CanteenXmlContentProviderException;



	public static class CanteenXmlContentProviderException extends Exception {
		private static final long serialVersionUID = 8535774001586402630L;
		public String messageForUser;
		public CanteenXmlContentProviderException(String message, String messageForUser) {
			super(message);
			this.messageForUser = messageForUser;
		}
	}

}
