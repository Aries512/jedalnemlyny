package sk.aries512.jedalnemlyny;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import sk.aries512.jedalnemlyny.utils.FileUtils;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.content.Context;

public class CanteenXmlProviderLocal implements CanteenXmlProvider {

	@Override
	public String getXmlContent(Context context) throws CanteenXmlContentProviderException {
		Utils.logv("reading local xml");
		try {
			return FileUtils.getFileContent(new File(MyApp.getInternalDataDir(), MyApp.canteenXmlFileName));
		} catch (FileNotFoundException e) {
			Utils.logv("local xml reading failed - file not found, reason", e.getMessage());
			throw new CanteenXmlContentProviderException("File not found.", null);
		} catch (IOException e) {
			Utils.logv("local xml reading failed - io exception, reason", e.getMessage());
			throw new CanteenXmlContentProviderException("IOException while reading file.", null);
		}
	}

}
