package sk.aries512.jedalnemlyny;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import sk.aries512.jedalnemlyny.utils.FileUtils;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CanteenXmlProviderRemote implements CanteenXmlProvider {

	@Override
	public String getXmlContent(Context context) throws CanteenXmlContentProviderException {

		Utils.logv("remote xml fetching started");

		if (!isConnectionAvailable(context)) {
			Utils.logv("remote xml fetching failed: no internet connection");
			throw new CanteenXmlContentProviderException("Internet connection not available.", context.getString(R.string.message_internet_unavailable));
		}

		List<String> servers = PreferencesUtils.loadStringList(context, context.getString(R.string.prefkey_server_list_compressed));

		for (String server: servers) {

			Utils.logv("remote xml fetching from", server);

			try {
				FileUtils.downloadFile(new URL(server), new File(MyApp.getInternalDataDir(), MyApp.canteenXmlFileNameCompressed));
			} catch (IOException e) {
				Utils.logv("remote xml fetching failed from", server, "reason", e.getMessage());
				continue;
			}
			Utils.logv("remote xml fetching finished");

			try {
				FileUtils.decompressGzip(
					new File(MyApp.getInternalDataDir(), MyApp.canteenXmlFileNameCompressed),
					new File(MyApp.getInternalDataDir(), MyApp.canteenXmlFileName));
				return FileUtils.getFileContent(new File(MyApp.getInternalDataDir(), MyApp.canteenXmlFileName));
			} catch (IOException e) {
				// aaah i want C# null coalescing operator
				String msg = e.getMessage() == null ? "" : e.getMessage();
				throw new CanteenXmlContentProviderException(e.getClass() + ": " + msg, context.getString(R.string.message_save_fail));
			}
		}

		throw new CanteenXmlContentProviderException("Couldn't connect to any server.", context.getString(R.string.message_server_fail));
	}



	private boolean isConnectionAvailable(Context context) {
		if (!isNetworkAvailable(context)) {
			// when device is woken up and quickly opens app, wifi or data may not be available right away
			// so we better wait a bit and give it second chance to start working
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) { }
			if (!isNetworkAvailable(context)) {
				return false;
			}
		}
		return true;
	}



	// http://stackoverflow.com/questions/4238921/android-detect-whether-there-is-an-internet-connection-available
	private boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
