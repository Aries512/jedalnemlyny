package sk.aries512.jedalnemlyny;

import org.acra.ACRA;
import org.acra.ACRAConstants;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Based on EmailIntentSender from ACRA.
 * I didn't like the way it's done (default app chooser title,
 * unspecified type of message, etc.), but forking whole ACRA is kinda overkill.
 * A bit of ctrl+c ctrl+v programming can't kill anybody... HEY WHAT IS THAT AAAAAAARRHGHGHGHFKHLK
 */
public class EmailReportSender implements ReportSender {

	private final Context mContext;

	public EmailReportSender(Context ctx) {
		mContext = ctx;
	}



	@Override
	public void send(CrashReportData errorContent) throws ReportSenderException {

		Utils.logi("crashed, sending report");

		final String subject = mContext.getPackageName() + " Crash Report";
		final String body = buildBody(errorContent);

		String uriText =
			"mailto:" + ACRA.getConfig().mailTo() +
			"?subject=" + Uri.parse(subject) +
			"&body=" + Uri.parse(body);
		Uri uri = Uri.parse(uriText);

		Intent mailIntent = new Intent(Intent.ACTION_SENDTO, uri);
		mailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mContext.startActivity(mailIntent);
	}



	private String buildBody(CrashReportData errorContent) {
		ReportField[] fields = ACRA.getConfig().customReportContent();
		if(fields.length == 0) {
			fields = ACRAConstants.DEFAULT_MAIL_REPORT_FIELDS;
		}

		final StringBuilder builder = new StringBuilder();
		for (ReportField field : fields) {
			builder.append(field.toString()).append("=");
			builder.append(errorContent.get(field));
			builder.append('\n');
		}
		return builder.toString();
	}
}
