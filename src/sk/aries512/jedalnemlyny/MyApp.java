package sk.aries512.jedalnemlyny;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import sk.aries512.jedalnemlyny.CanteenXmlProvider.CanteenXmlContentProviderException;
import sk.aries512.jedalnemlyny.model.Canteen;
import sk.aries512.jedalnemlyny.model.MetaData;
import sk.aries512.jedalnemlyny.model.XmlParser;
import sk.aries512.jedalnemlyny.model.XmlParser.XmlParserException;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

//ACRA crash reporting
@ReportsCrashes(
	formKey = "", // This is required for backward compatibility but not used
	mailTo = "matej.kopernicky@gmail.com", // TODO: change mail address!
	//mode = ReportingInteractionMode.TOAST,
	mode = ReportingInteractionMode.DIALOG,
	resToastText = R.string.crash_toast_text,
	forceCloseDialogAfterToast = true,
	resNotifTickerText = R.string.crash_notif_ticker_text,
	resNotifTitle = R.string.crash_notif_title,
	resNotifText = R.string.crash_notif_text,
	resNotifIcon = android.R.drawable.stat_notify_error, // optional. default is a warning sign
	resDialogText = R.string.crash_dialog_text,
	resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
	resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
	resDialogCommentPrompt = R.string.crash_dialog_comment_prompt // optional. when defined, adds a user text field input with this text resource as a label
	//resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
	)
public class MyApp extends Application {



	public static final String LOG_TAG = "JedalneMlyny";

	private static TimeZone timeZone = TimeZone.getDefault();

	/**
	 * Whether data are currently being refreshed. This must be general state
	 * of the whole app, because user can press back button right after
	 * initiating refresh, which will destroy main activity.
	 * However, we want to preserve refresh action anyway.
	 */
	public static boolean refreshingNow = false;

	/**
	 * Current data available to be shown in UI.
	 */
	private static List<Canteen> canteens = new ArrayList<Canteen>();
	private static final Object canteenLock = new Object();
	public static List<Canteen> getCanteens() {
		synchronized (canteenLock) {
			return canteens;
		}
	}
	public static void setCanteens(List<Canteen> canteens) {
		synchronized (canteenLock) {
			MyApp.canteens = canteens;
		}
	}

	public static final String canteenXmlFileName = "canteenData.xml";
	public static final String canteenXmlFileNameCompressed = "canteenData.xml.gz";

	/**
	 * Value of current text size delta is stored in preferences.
	 * This variable exists so preferences don't have to be open for each fragment onCreate method.
	 */
	public static int textSizeDeltaCurrent = 0;
	public static final int[] textSizeDeltas = new int[] { -2, 0, 2, 4 };

	/**
	 * Hidden categories list is stored in preferences.
	 * This variable exists so preferences don't have to be open for each fragment's onCreate method.
	 */
	public static List<String> hiddenCategories;



	private static MyApp instance = null;
	public static MyApp getInstance() {
		checkInstance();
		return instance;
	}

	private static void checkInstance() {
		// this actually shouldn't happen
		if (instance == null) {
			throw new IllegalStateException("Application not created yet!");
		}
	}



	public static TimeZone getAppTimeZone() {
		return timeZone;
	}



	public static String getInternalDataDir() {
		return getInstance().getFilesDir().getAbsolutePath();
	}



	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		setupACRA();
		setupTimezone();
		initPreferences();
		loadLocalXml();
	}



	private void setupACRA() {
		ACRA.init(this);
		ACRA.getErrorReporter().setReportSender(new EmailReportSender(this));
	}



	private void setupTimezone() {
		for (String zoneId: TimeZone.getAvailableIDs()) {
			if (zoneId.contains("Bratislava") || zoneId.contains("Prague") || zoneId.contains("Budapest") || zoneId.contains("Vienna")) {
				timeZone = TimeZone.getTimeZone(zoneId);
				break;
			}
		}
	}



	private void initPreferences() {

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

		if (sp.getBoolean(getString(R.string.prefkey_first_run), true)) {

			sp.edit().putBoolean(getString(R.string.prefkey_first_run), false).commit();

			// TODO: update default servers
			PreferencesUtils.saveStringList(getInstance(), getString(R.string.prefkey_server_list_compressed),
				"http://davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml.gz",
				"http://jedalne.host-ed.me/jedalne/jedalne.xml.gz");
			PreferencesUtils.saveStringList(getInstance(), getString(R.string.prefkey_server_list_uncompressed),
				"http://davinci.fmph.uniba.sk/~kopernicky7/jedalne/jedalne.xml",
				"http://jedalne.host-ed.me/jedalne/jedalne.xml");

			PreferencesUtils.saveInt(getInstance(), getString(R.string.prefkey_update_timeout), MetaData.TIMEOUT_SECONDS_DEFAULT);
			PreferencesUtils.saveInt(getInstance(), getString(R.string.prefkey_text_size_delta), 0);

			PreferencesUtils.saveStringList(this, getString(R.string.prefkey_categories_all),
				"Hlavné jedlá",
				"Nadštandardné jedlá",
				"Polievky",
				"Šaláty",
				"Prílohy",
				"Nápoje",
				"Raňajky",
				"Studené jedlá");
			PreferencesUtils.saveStringList(this, getString(R.string.prefkey_categories_hidden),
				"Prílohy",
				"Nápoje",
				"Raňajky",
				"Studené jedlá");
		}

		textSizeDeltaCurrent = PreferencesUtils.loadInt(getInstance(), getString(R.string.prefkey_text_size_delta), 0);
		hiddenCategories = PreferencesUtils.loadStringList(this, getString(R.string.prefkey_categories_hidden));

	}



	private void loadLocalXml() {
		String xml = null;
		try {
			xml = new CanteenXmlProviderLocal().getXmlContent(this);
		} catch (CanteenXmlContentProviderException e) {
			return;
		}
		if (Utils.hasValue(xml)) {
			try {
				MyApp.canteens = new XmlParser(xml).getCanteenData();
			} catch (XmlParserException e) { }


		}
	}

}
