package sk.aries512.jedalnemlyny.model;

import java.util.Calendar;
import java.util.List;

public class Canteen {

	public String name;
	public String source;
	public Calendar lastUpdate;
	public List<DailyMenu> dailyMenus;

}
