package sk.aries512.jedalnemlyny.model;

import java.util.Calendar;
import java.util.List;

public class DailyMenu {

	public static final int SHOW_DAILY_MENU_UNTIL_HOUR = 20;

	public Calendar day;
	public List<MenuCategory> categories;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (MenuCategory cat: categories) {
			sb.append(cat.toString() + "\n");
		}
		return sb.toString();
	}

}
