package sk.aries512.jedalnemlyny.model;

import java.util.List;

public class MenuCategory {

	public String name;
	public List<MenuItem> items;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(name == null ? "" : name.toUpperCase());
		sb.append("\n");
		for (MenuItem item: items) {
			sb.append(item.toString() + "\n");
		}
		return sb.toString();
	}

}
