package sk.aries512.jedalnemlyny.model;

public class MenuItem {

	public String name;

	/**
	 * Stored as String, because units can vary (weight, volume, etc., or no data).
	 */
	public String quantity;

	/**
	 * Identifiers of allergens, currently using only numbers, but things can change.
	 */
	public String allergenes;

	public String priceStudent;

	public String priceFull;

	@Override
	public String toString() {
		return
			(name == null ? "" : name) + " | " +
			(quantity == null ? "" : quantity) + " | " +
			(allergenes == null ? "" : allergenes) + " | " +
			(priceStudent == null ? "" : priceStudent) + " | " +
			(priceFull == null ? "" : priceFull);
	}

}
