package sk.aries512.jedalnemlyny.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import sk.aries512.jedalnemlyny.R;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import android.content.Context;

public class MetaData {

	public static final int TIMEOUT_SECONDS_DEFAULT = 1800;

	public Calendar creationDateTime;
	public int timeoutSeconds = TIMEOUT_SECONDS_DEFAULT;
	public String info;
	public List<ServerLocation> serverLocations = new ArrayList<ServerLocation>();
	public List<ColumnName> columnNames = new ArrayList<ColumnName>();
	public List<String> notLoaded = new ArrayList<String>();
	public List<String> usedOld = new ArrayList<String>();

	public static class ColumnName {
		public String full;
		public String shortt;
	}

	public static class ServerLocation {
		public String compressed;
		public String uncompressed;
	}



	public void saveToPreferences(Context context) {

		PreferencesUtils.saveInt(context,
			context.getString(R.string.prefkey_update_timeout),
			timeoutSeconds);

		String[] compressed = new String[serverLocations.size()];
		String[] uncompressed = new String[serverLocations.size()];
		for (int i = 0; i < compressed.length; ++i) {
			compressed[i] = serverLocations.get(i).compressed;
			uncompressed[i] = serverLocations.get(i).uncompressed;
		}
		PreferencesUtils.saveStringList(context,
			context.getString(R.string.prefkey_server_list_compressed),
			compressed);
		PreferencesUtils.saveStringList(context,
			context.getString(R.string.prefkey_server_list_uncompressed),
			uncompressed);
	}

}
