package sk.aries512.jedalnemlyny.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sk.aries512.jedalnemlyny.model.MetaData.ColumnName;
import sk.aries512.jedalnemlyny.model.MetaData.ServerLocation;
import sk.aries512.jedalnemlyny.utils.DateTimeUtils;

public class XmlParser {

	public static class XmlParserException extends Exception {
		private static final long serialVersionUID = -7583021012982540759L;
		public XmlParserException(String message) {
			super(message);
		}
	}



	private Document doc = null;



	public XmlParser(String xml) throws XmlParserException {

		if (xml == null) {
			throw new IllegalArgumentException();
		}

		try {
			InputStream stream = new ByteArrayInputStream(xml.getBytes());
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(stream);
		} catch (Exception e) {
			throw new XmlParserException("Error when creating parser");
		}
	}



	public MetaData getMetaData() throws XmlParserException {

		NodeList metaNodes = doc.getElementsByTagName("meta");
		if (metaNodes.getLength() < 1) {
			throw new XmlParserException("Meta node not present");
		}

		Node metaNode = metaNodes.item(0);
		MetaData metaData = new MetaData();

		for (int i = 0; i < metaNode.getChildNodes().getLength(); ++i) {

			Node node = metaNode.getChildNodes().item(i);
			String name = node.getNodeName();
			String value = node.getTextContent();

			if (name.equals("datetime")) {
				try {
					metaData.creationDateTime = DateTimeUtils.parseFull(value);
				} catch (ParseException e) { }
			} else if (name.equals("timeout")) {
				metaData.timeoutSeconds = Integer.parseInt(value);
			} else if (name.equals("info")) {
				metaData.info = value;
			} else if (name.equals("columnNames")) {
				metaData.columnNames = getColumnNames(node.getChildNodes());
			} else if (name.equals("servers")) {
				metaData.serverLocations = getServers(node.getChildNodes());
			} else if (name.equals("errors")) {
				for (int j = 0; j < node.getChildNodes().getLength(); ++j) {
					Node cNode = node.getChildNodes().item(j);
					String cName = cNode.getNodeName();
					if (cName.equals("notLoaded")) {
						metaData.notLoaded = getErrorList(cNode.getChildNodes());
					} else if (cName.equals("usedOld")) {
						metaData.usedOld = getErrorList(cNode.getChildNodes());
					}
				}
			}
		}

		return metaData;

	}



	private List<String> getErrorList(NodeList nodes) {
		List<String> res = new ArrayList<String>();
		for (int i = 0; i < nodes.getLength(); ++i) {
			if (nodes.item(i).getNodeName().equals("name")) {
				res.add(nodes.item(i).getTextContent());
			}
		}
		return res;
	}



	private List<ColumnName> getColumnNames(NodeList nodes) {

		List<ColumnName> res = new ArrayList<ColumnName>();

		for (int i = 0; i < nodes.getLength(); ++i) {
			if (nodes.item(i).getNodeName().equals("columnName")) {

				ColumnName cn = new ColumnName();
				NodeList names = nodes.item(i).getChildNodes();

				for (int j = 0; j < names.getLength(); ++j) {
					Node field = names.item(j);
					if (field.getNodeName().equals("full")) {
						cn.full = field.getTextContent();
					}
					else if (field.getNodeName().equals("short")) {
						cn.shortt = field.getTextContent();
					}
				}
				res.add(cn);
			}
		}

		return res;
	}



	private List<ServerLocation> getServers(NodeList nodes) {

		List<ServerLocation> res = new ArrayList<ServerLocation>();

		for (int i = 0; i < nodes.getLength(); ++i) {
			if (nodes.item(i).getNodeName().equals("server")) {

				ServerLocation sl = new ServerLocation();
				NodeList fields = nodes.item(i).getChildNodes();

				for (int j = 0; j < fields.getLength(); ++j) {
					Node field = fields.item(j);
					if (field.getNodeName().equals("compressed")) {
						sl.compressed = field.getTextContent();
					}
					else if (field.getNodeName().equals("uncompressed")) {
						sl.uncompressed = field.getTextContent();
					}
				}
				res.add(sl);
			}
		}

		return res;
	}



	public List<Canteen> getCanteenData() {

		List<Canteen> canteens = new ArrayList<Canteen>();
		NodeList canteenNodes = doc.getElementsByTagName("canteen");

		for (int i = 0; i < canteenNodes.getLength(); ++i) {
			canteens.add(getCanteen(canteenNodes.item(i)));
		}

		return canteens;
	}



	private Canteen getCanteen(Node node) {

		Canteen canteen = new Canteen();
		canteen.dailyMenus = new ArrayList<DailyMenu>();

		for (int i = 0; i < node.getChildNodes().getLength(); ++i) {

			Node cNode = node.getChildNodes().item(i);
			String name = cNode.getNodeName();
			String value = cNode.getTextContent();

			if (name.equals("name")) {
				canteen.name = value;
			} else if (name.equals("source")) {
				canteen.source = value;
			} else if (name.equals("lastUpdate")) {
				try {
					canteen.lastUpdate = DateTimeUtils.parseFull(value);
				} catch (ParseException e) { }
			} else if (name.equals("dailyMenu")) {
				canteen.dailyMenus.add(getDailyMenu(cNode));
			}
		}

		return canteen;
	}



	private DailyMenu getDailyMenu(Node node) {

		DailyMenu menu = new DailyMenu();
		menu.categories = new ArrayList<MenuCategory>();

		try {
			menu.day = DateTimeUtils.parseSimple(node.getAttributes().getNamedItem("date").getTextContent());
		} catch (DOMException e) {
		} catch (ParseException e) {
		}

		for (int i = 0; i < node.getChildNodes().getLength(); ++i) {
			Node cNode = node.getChildNodes().item(i);
			if (cNode.getNodeName().equals("category")) {
				MenuCategory category = new MenuCategory();
				category.name = cNode.getAttributes().getNamedItem("name").getTextContent();
				category.items = getMenuItems(cNode.getChildNodes());
				menu.categories.add(category);
			}
		}

		return menu;
	}



	private List<MenuItem> getMenuItems(NodeList nodes) {

		List<MenuItem> items = new ArrayList<MenuItem>();

		for (int i = 0; i < nodes.getLength(); ++i) {
			if (nodes.item(i).getNodeName().equals("item")) {

				MenuItem item = new MenuItem();
				items.add(item);

				for (int j = 0; j < nodes.item(i).getChildNodes().getLength(); ++j) {

					Node field = nodes.item(i).getChildNodes().item(j);
					String name = field.getNodeName();
					String value = field.getTextContent();

					if (name.equals("name")) {
						item.name = value;
					} else if (name.equals("quantity")) {
						item.quantity = value;
					} else if (name.equals("allergens")) {
						item.allergenes = value;
					} else if (name.equals("price")) {
						item.priceStudent = value;
					} else if (name.equals("priceFull")) {
						item.priceFull = value;
					}
				}
			}
		}

		return items;
	}

}
