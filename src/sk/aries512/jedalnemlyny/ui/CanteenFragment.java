package sk.aries512.jedalnemlyny.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import sk.aries512.jedalnemlyny.MyApp;
import sk.aries512.jedalnemlyny.R;
import sk.aries512.jedalnemlyny.model.Canteen;
import sk.aries512.jedalnemlyny.model.DailyMenu;
import sk.aries512.jedalnemlyny.utils.DateTimeUtils;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public class CanteenFragment extends Fragment {



	public static CanteenFragment newInstance(int position) {
		CanteenFragment fragment = new CanteenFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("canteen_position", position);
		fragment.setArguments(bundle);
		return fragment;
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}





	private Context context;

	private Spinner spinner;

	/**
	 * Spinner's onItemSelectedListener is fired too when selection is changed programatically.
	 * However, we want some actions to be taken only when selection is made by user.
	 * This should help us to differentiate between the cases.
	 * Must be set to true each time before we call setSelection on spinner and then reset when event is consumed.
	 */
	private boolean spinnerSelectionChangedProgramatically = false;

	/**
	 * Stores selected day in the spinner for each canteen.
	 * This is used to preserve selection after refresh (if such day is contained in canteen's menu).
	 */
	private static HashMap<String, Calendar> selectedDay = new HashMap<String, Calendar>();

	/**
	 * Days displayed in spinner. Should always correspond to menus available in the canteen.
	 */
	private List<Calendar> days;





	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		context = getActivity();
		final DailyMenuSelectionManager selectionManager = (DailyMenuSelectionManager) getActivity();

		View rootView = inflater.inflate(R.layout.canteen_fragment, null, false);

		// spinner = (Spinner) rootView.findViewById(R.id.canteen_day_spinner);
		RelativeLayout spinnerWrapper = (RelativeLayout) rootView.findViewById(R.id.canteen_day_spinner_wrapper);
		spinner = new Spinner(context);
		spinnerWrapper.addView(spinner);

		final ListView list = (ListView) rootView.findViewById(R.id.canteen_menu_list);

		int position = getArguments().getInt("canteen_position");
		final Canteen canteen = MyApp.getCanteens().get(position);

		setSpinner(spinner, list, canteen, selectionManager);

		return rootView;
	}



	private void setSpinner(Spinner spinner, final ListView list,
		final Canteen canteen, final DailyMenuSelectionManager selectionManager) {

		List<String> daysStr = new ArrayList<String>();
		days = new ArrayList<Calendar>();
		int selected = -1;
		Calendar showDay = DateTimeUtils.getDayToShow(DateTimeUtils.now(), DailyMenu.SHOW_DAILY_MENU_UNTIL_HOUR);
		Calendar last = selectedDay.get(canteen.name);
		for (DailyMenu menu: canteen.dailyMenus) {
			// if there was any day selected before, it will be preserved (if it is still available)
			// otherwise, most appropriate day will be selected by default
			if ((last != null && DateTimeUtils.areSameDay(menu.day, last)) ||
				(selected < 0 && DateTimeUtils.areSameDay(menu.day, showDay))) {
				selected = daysStr.size();
			}
			days.add(menu.day);
			daysStr.add(DateTimeUtils.formatDateUiRich(menu.day));
		}
		selected = Math.max(0, selected);

		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
			getActivity(), android.R.layout.simple_spinner_item, daysStr);
		spinnerAdapter.setDropDownViewResource(R.layout.dropdown_item);
		spinner.setAdapter(spinnerAdapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Calendar selected = days.get(arg2);
				selectedDay.put(canteen.name, selected);

				list.setAdapter(new CanteenMenuListAdapter(
					context, canteen.dailyMenus.get(arg2), getFooterText(canteen)));
				if (!spinnerSelectionChangedProgramatically) {
					selectionManager.daySelected(selected);
				}
				spinnerSelectionChangedProgramatically = false;
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) { }
		});
		if (canteen.dailyMenus.size() > 0) {
			spinnerSelectionChangedProgramatically = true;
			spinner.setSelection(selected);
		}

	}



	private String getFooterText(Canteen canteen) {
		// TODO: remove or change footer in final release?
		String footer = canteen.name;
		String lastUpdateStr = PreferencesUtils.loadString(context, getString(R.string.prefkey_last_update), "");
		if (Utils.hasValue(lastUpdateStr)) {
			Calendar lastUpdate = null;
			try {
				lastUpdate = DateTimeUtils.parseFull(lastUpdateStr);
			} catch (ParseException e) { }
			footer += "\nPosledná aktualizácia: " + DateTimeUtils.formatFullUi(lastUpdate);
		}
		return footer + "\nDáta z: " + DateTimeUtils.formatFullUi(canteen.lastUpdate);
	}



	public void setSelectedDay(Calendar day) {
		if (days == null || day == null || spinner == null || DateTimeUtils.areSameDay(days.get(spinner.getSelectedItemPosition()), day)) {
			return;
		}
		for (int i = 0; i < days.size(); ++i) {
			if (DateTimeUtils.areSameDay(day, days.get(i))) {
				spinnerSelectionChangedProgramatically = true;
				spinner.setSelection(i);
				break;
			}
		}
	}

}
