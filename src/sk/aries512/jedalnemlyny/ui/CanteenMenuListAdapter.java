package sk.aries512.jedalnemlyny.ui;

import java.util.ArrayList;
import java.util.List;
import sk.aries512.jedalnemlyny.MyApp;
import sk.aries512.jedalnemlyny.R;
import sk.aries512.jedalnemlyny.model.DailyMenu;
import sk.aries512.jedalnemlyny.model.MenuCategory;
import sk.aries512.jedalnemlyny.model.MenuItem;
import sk.aries512.jedalnemlyny.utils.Dialogs;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CanteenMenuListAdapter extends BaseAdapter {



	private static class ListItem {

		public enum ListItemType {
			Title,
			Item,
			SeparatorLine,
			SeparatorSpace,
			Footer
		}

		private ListItemType type;
		private String name;
		private String quantity;
		private String allergenes;
		private String price;



		private ListItem(ListItemType type, String name, String quantity, String allergenes, String priceFull, String priceStudent) {
			this.type = type;
			this.name = name;
			this.quantity = quantity;
			this.allergenes = null;
			if (Utils.hasValue(allergenes)) {
				this.allergenes = "alerg.: " + allergenes;
			}
			this.price = getPriceString(priceFull, priceStudent);
		}

		private String getPriceString(String priceFull, String priceStudent) {
			if (!Utils.hasValue(priceFull) && !Utils.hasValue(priceStudent)) {
				return null;
			}
			priceStudent = Utils.hasValue(priceStudent) ? priceStudent + "€" : "";
			priceFull = Utils.hasValue(priceFull) ? priceFull + "€" : "";
			return priceStudent + (Utils.allHaveValue(priceStudent, priceFull) ? " / " : "") + priceFull;
		}



		public static ListItem makeItem(MenuItem item) {
			return new ListItem(ListItemType.Item, item.name, item.quantity, item.allergenes, item.priceFull, item.priceStudent);
		}

		public static ListItem makeTitle(String text) {
			return new ListItem(ListItemType.Title, text, null, null, null, null);
		}

		public static ListItem makeFooter(String text) {
			return new ListItem(ListItemType.Footer, text, null, null, null, null);
		}

		public static ListItem makeSeparatorLine() {
			return new ListItem(ListItemType.SeparatorLine, null, null, null, null, null);
		}

		public static ListItem makeSeparatorSpace() {
			return new ListItem(ListItemType.SeparatorSpace, null, null, null, null, null);
		}

	}



	private List<ListItem> listItems;
	private Context context;
	private LayoutInflater inflater;

	public CanteenMenuListAdapter(Context context, DailyMenu menu, String footerText) {

		this.context = context;
		this.inflater = LayoutInflater.from(context);

		listItems = new ArrayList<ListItem>();
		for (MenuCategory cat: menu.categories) {
			if (Utils.containsIgnoreCase(MyApp.hiddenCategories, cat.name)) {
				continue;
			}
			listItems.add(ListItem.makeTitle(cat.name));
			for (int i = 0; i < cat.items.size(); ++i) {
				listItems.add(ListItem.makeItem(cat.items.get(i)));
				listItems.add(i == cat.items.size() - 1 ? ListItem.makeSeparatorSpace() : ListItem.makeSeparatorLine());
			}
		}
		listItems.add(ListItem.makeFooter(footerText));
	}



	@Override
	public int getCount() {
		return listItems.size();

	}

	@Override
	public Object getItem(int position) {
		return listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return ListItem.ListItemType.values().length;
	};

	@Override
	public int getItemViewType(int position) {
		return listItems.get(position).type.ordinal();
	};

	// to make it non-clickable
	@Override
	public boolean isEnabled(int position) {
		return false;
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ListItem item = (ListItem)getItem(position);

		if (convertView == null) {
			ViewHolder holder = new ViewHolder();
			switch (item.type) {
			case SeparatorLine:
				convertView = makeSeparatorLine(parent);
				break;
			case SeparatorSpace:
				convertView = makeSeparatorSpace(parent);
				break;
			case Title:
				convertView = holder.tvName = makeTitle(parent);
				break;
			case Item:
				convertView = makeMenuItem(context, parent);
				holder.tvName = (TextView)convertView.findViewById(R.id.menu_item_name);
				holder.tvQuantity = (TextView)convertView.findViewById(R.id.menu_item_quantity);
				holder.tvAllergenes = (TextView)convertView.findViewById(R.id.menu_item_allergenes);
				holder.tvPrice = (TextView)convertView.findViewById(R.id.menu_item_price);
				break;
			case Footer:
				convertView = holder.tvName = makeFooter(parent);
				break;
			default: return null;
			}

			holder.setViewProperties(item);
			convertView.setTag(holder);

		} else {
			if (convertView.getTag() != null) {
				((ViewHolder)convertView.getTag()).setViewProperties(item);
			}
		}

		return convertView;
	}



	private class ViewHolder {
		private TextView tvName;
		private TextView tvQuantity;
		private TextView tvAllergenes;
		private TextView tvPrice;

		public void setViewProperties(ListItem item) {
			TextView[] tvs = new TextView[] { tvName, tvQuantity, tvAllergenes, tvPrice };
			String[] vals = new String[] { item.name, item.quantity, item.allergenes, item.price };
			for (int i = 0; i < tvs.length; ++i) {
				if (tvs[i] != null) {
					tvs[i].setText(vals[i]);
					tvs[i].setVisibility(Utils.hasValue(tvs[i].getText().toString()) ? View.VISIBLE : View.GONE);
				}
			}
		}
	}



	private View makeSeparatorLine(ViewGroup vg) {
		return inflater.inflate(R.layout.menu_separator_line, vg, false);
	}

	private View makeSeparatorSpace(ViewGroup vg) {
		return inflater.inflate(R.layout.menu_separator_space, vg, false);
	}

	private TextView makeTitle(ViewGroup vg) {
		TextView tv = (TextView) inflater.inflate(R.layout.menu_title_text, vg, false);
		tv.setTextSize(18 + MyApp.textSizeDeltaCurrent);
		return tv;
	}

	private LinearLayout makeMenuItem(final Context context, ViewGroup vg) {

		LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.menu_item, vg, false);

		TextView tvName = (TextView)layout.findViewById(R.id.menu_item_name);
		tvName.setTextSize(15 + MyApp.textSizeDeltaCurrent);

		int smallSize = 13 + MyApp.textSizeDeltaCurrent;

		TextView tvQuantity = (TextView)layout.findViewById(R.id.menu_item_quantity);
		tvQuantity.setTextSize(smallSize);

		TextView tvAllergenes = (TextView)layout.findViewById(R.id.menu_item_allergenes);
		tvAllergenes.setTextSize(smallSize);
		tvAllergenes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialogs.showMessage(context, context.getString(R.string.allergenes_list));
			}
		});

		TextView tvPrice = (TextView)layout.findViewById(R.id.menu_item_price);
		tvPrice.setTextSize(smallSize);

		return layout;
	}

	private TextView makeFooter(ViewGroup vg) {
		TextView tv = (TextView) inflater.inflate(R.layout.menu_footer_text, vg, false);
		tv.setTextSize(13 + MyApp.textSizeDeltaCurrent);
		return tv;
	}

}
