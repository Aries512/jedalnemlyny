package sk.aries512.jedalnemlyny.ui;

import sk.aries512.jedalnemlyny.MyApp;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

public class CanteenPagerAdapter extends FragmentPagerAdapter {

	public CanteenPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return CanteenFragment.newInstance(position);
	}

	@Override
	public int getItemPosition(Object object) {
		// this is necessary if we want to be able to change pages dynamically
		return POSITION_NONE;
	}

	@Override
	public int getCount() {
		return MyApp.getCanteens().size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return MyApp.getCanteens().get(position).name;
	}



	// http://stackoverflow.com/a/15261142/1732455

	SparseArray<CanteenFragment> registeredFragments = new SparseArray<CanteenFragment>();

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		CanteenFragment fragment = (CanteenFragment) super.instantiateItem(container, position);
		registeredFragments.put(position, fragment);
		return fragment;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		registeredFragments.remove(position);
		super.destroyItem(container, position, object);
	}

	public CanteenFragment getRegisteredFragment(int position) {
		return registeredFragments.get(position);
	}

}
