package sk.aries512.jedalnemlyny.ui;

import java.util.Calendar;

public interface DailyMenuSelectionManager {

	public void daySelected(Calendar day);

}
