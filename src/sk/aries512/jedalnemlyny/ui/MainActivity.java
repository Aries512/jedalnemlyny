package sk.aries512.jedalnemlyny.ui;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import sk.aries512.jedalnemlyny.CanteenXmlProvider;
import sk.aries512.jedalnemlyny.CanteenXmlProvider.CanteenXmlContentProviderException;
import sk.aries512.jedalnemlyny.CanteenXmlProviderRemote;
import sk.aries512.jedalnemlyny.MyApp;
import sk.aries512.jedalnemlyny.R;
import sk.aries512.jedalnemlyny.model.Canteen;
import sk.aries512.jedalnemlyny.model.DailyMenu;
import sk.aries512.jedalnemlyny.model.MenuCategory;
import sk.aries512.jedalnemlyny.model.MetaData;
import sk.aries512.jedalnemlyny.model.XmlParser;
import sk.aries512.jedalnemlyny.model.XmlParser.XmlParserException;
import sk.aries512.jedalnemlyny.utils.DateTimeUtils;
import sk.aries512.jedalnemlyny.utils.Dialogs;
import sk.aries512.jedalnemlyny.utils.PreferencesUtils;
import sk.aries512.jedalnemlyny.utils.Utils;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, DailyMenuSelectionManager {



	private ViewPager viewPager;
	private CanteenPagerAdapter pagerAdapter;
	private ActionBar actionBar;
	private MenuItem refreshItem;

	private static int REFRESH_WAIT_TIME_SECONDS;
	private long lastManualRefreshMillis;
	private String lastTabName;

	private static final String REFRESH_BROADCAST_ACTION = "refresh_broadcast_action";
	private RefreshResultListener refreshListener;



	private void init() {
		REFRESH_WAIT_TIME_SECONDS = 5;
		lastManualRefreshMillis = 0;
		lastTabName = PreferencesUtils.loadString(this, getString(R.string.prefkey_last_tab_name), "");
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		init();

		setContentView(R.layout.main_activity);

		viewPager = (ViewPager)findViewById(R.id.pager);
		pagerAdapter = new CanteenPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(pagerAdapter);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);
		allowOverflowMenuHack();

		updateUi(true);

		IntentFilter filter = new IntentFilter(REFRESH_BROADCAST_ACTION);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		refreshListener = new RefreshResultListener();
		LocalBroadcastManager.getInstance(this).registerReceiver(refreshListener, filter);

	}



	// http://stackoverflow.com/questions/9286822/how-to-force-use-of-overflow-menu-on-devices-with-menu-button
	private void allowOverflowMenuHack() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception ex) { }
	}



	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		lastTabName = arg0.getText().toString();
		viewPager.setCurrentItem(arg0.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
	}



	// we refresh data when user opens app (if data are old enough)
	//
	// however, using onResume is not a good idea for 2 reasons:
	// 1. onResume doesn't necessarily mean that app is visible to user
	//    (for example, it can be called when user is on lock screen after unlocking
	// 2. when onResume is called, UI is not always fully drawn, which means that
	//    refresh animation wouldn't be run if started from onResume
	//
	// this is why we refresh data from onWindowFocusChanged, however this isn't the
	// most clever idea either, because it is called not only when app is shown to user,
	// but also when it is on background and gains focus (after closing overlay dialog etc.)
	//
	// so we combine both approaches and refresh data after app is both resumed and gains focus afterwards

	private boolean wasRecentlyResumed = false;

	@Override
	protected void onResume() {
		super.onResume();
		wasRecentlyResumed = true;
	};

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (wasRecentlyResumed && !MyApp.refreshingNow) {

			wasRecentlyResumed = false;

			// update UI first, to show up-to-date list of days in case:
			// 1. downloading fails or is not done at all, so no new list is shown
			// 2. previous list was created and shown yesterday or even more then day ago
			// in that case we need to refresh UI to show and select appropriate up-to-date days
			updateUi(true);

			if (shouldDownloadXml()) {
				refreshData(new CanteenXmlProviderRemote(), true);
			}
		}
	}



	@Override
	protected void onPause() {
		super.onPause();
		if (Utils.hasValue(lastTabName)) {
			PreferencesUtils.saveString(this, getString(R.string.prefkey_last_tab_name), lastTabName);
		}
	}



	@Override
	protected void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(refreshListener);
		super.onDestroy();
	}



	@Override
	public void daySelected(Calendar day) {
		setSelectedDayInFragments(day);
	}

	private void setSelectedDayInFragments(Calendar day) {
		for (int i = 0; i < pagerAdapter.registeredFragments.size(); i++) {
			pagerAdapter.registeredFragments.get(pagerAdapter.registeredFragments.keyAt(i)).setSelectedDay(day);
		}
	}



	private boolean shouldDownloadXml() {
		String lastUpdateStr = PreferencesUtils.loadString(this, getString(R.string.prefkey_last_update), "");
		if (Utils.hasValue(lastUpdateStr)) {
			Calendar update = null;
			try {
				update = DateTimeUtils.parseFull(lastUpdateStr);
			} catch (ParseException e) {
				return true;
			}
			int timeoutSeconds = PreferencesUtils.loadInt(this, getString(R.string.prefkey_update_timeout), MetaData.TIMEOUT_SECONDS_DEFAULT);
			update.add(Calendar.SECOND, timeoutSeconds);
			return update.before(DateTimeUtils.now());
		} else {
			return true;
		}
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity, menu);
		this.refreshItem = menu.findItem(R.id.menu_refresh);
		if (MyApp.refreshingNow) {
			startRefreshAnimation();
		}
		return super.onCreateOptionsMenu(menu);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.menu_refresh) {
			long now = System.currentTimeMillis();
			// prevent user mashing refresh button in an insane, inappropiate or rude manner
			// in order to allow obtaining remote data only in reasonaly distant time intervals,
			// as humble upbringing instructs us to do
			if (!MyApp.refreshingNow && lastManualRefreshMillis + REFRESH_WAIT_TIME_SECONDS * 1000 < now) {
				refreshData(new CanteenXmlProviderRemote(), false);
				lastManualRefreshMillis = System.currentTimeMillis();
			}
		}

		if (item.getItemId() == R.id.menu_categories) {
			selectCategories();
		}

		if (item.getItemId() == R.id.menu_text_size) {
			selectTextSize();
		}

		if (item.getItemId() == R.id.menu_allergenes) {
			Dialogs.showMessage(this, getString(R.string.allergenes_list));
		}

		if (item.getItemId() == R.id.menu_crash_test) {
			int x = 0;
			@SuppressWarnings("unused")
			int y = 1 / x;
		}

		if (item.getItemId() == R.id.menu_about) {

		}

		return true;
	}



	private void selectTextSize() {

		int curr = PreferencesUtils.loadInt(this, getString(R.string.prefkey_text_size_delta), 0);
		int diff = Integer.MAX_VALUE;
		int selected = 0;
		for (int i = 0; i < MyApp.textSizeDeltas.length; ++i) {
			if (Math.abs(MyApp.textSizeDeltas[i] - curr) < diff) {
				diff = Math.abs(MyApp.textSizeDeltas[i] - curr);
				selected = i;
			}
		}

		new AlertDialog.Builder(this)
		.setCancelable(true)
		.setTitle(R.string.title_text_size)
		.setSingleChoiceItems(R.array.text_sizes, selected, null)
		.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				ListView lw = ((AlertDialog)dialog).getListView();
				int pos = lw.getCheckedItemPosition();
				MyApp.textSizeDeltaCurrent = MyApp.textSizeDeltas[pos];
				PreferencesUtils.saveInt(MainActivity.this,
					getString(R.string.prefkey_text_size_delta), MyApp.textSizeDeltaCurrent);

				updateUi(false);
			}
		})
		.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		})
		.show();
	}



	private void selectCategories() {

		final List<String> categories = PreferencesUtils.loadStringList(this, getString(R.string.prefkey_categories_all));
		final List<String> hidden = PreferencesUtils.loadStringList(this, getString(R.string.prefkey_categories_hidden));
		final boolean[] checked = new boolean[categories.size()];
		for (int i = 0; i < categories.size(); ++i) {
			checked[i] = !hidden.contains(categories.get(i));
		}

		new AlertDialog.Builder(this)
		.setCancelable(true)
		.setTitle(R.string.title_categories)
		.setMultiChoiceItems(categories.toArray(new String[categories.size()]), checked, new OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if (isChecked) {
					hidden.remove(categories.get(which));
				} else {
					hidden.add(categories.get(which));
				}
			}
		})
		.setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int whichButton) {
				PreferencesUtils.saveStringList(
					MainActivity.this, getString(R.string.prefkey_categories_hidden), hidden.toArray(new String[hidden.size()]));
				MyApp.hiddenCategories = hidden;
				updateUi(false);
			}
		})
		.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		})
		.show();
	}





	private void refreshData(final CanteenXmlProvider xmlProvider, final boolean showCurrentDay) {

		MyApp.refreshingNow = true;
		startRefreshAnimation();
		Utils.logv("refreshing data");

		new AsyncTask<Void, Void, RefreshResult>() {

			@Override
			protected RefreshResult doInBackground(Void... params) {
				String info = null;
				try {
					String xml = xmlProvider.getXmlContent(MainActivity.this);
					if (!Utils.hasValue(xml)) {
						return new RefreshResult(false, null, null, showCurrentDay);
					}
					XmlParser parser = new XmlParser(xml);
					final MetaData metaData = parser.getMetaData();
					metaData.saveToPreferences(MainActivity.this);
					info = metaData.info;
					List<Canteen> canteens = parser.getCanteenData();
					saveCategories(canteens);
					MyApp.setCanteens(canteens);
				} catch (CanteenXmlContentProviderException e) {
					return new RefreshResult(false, e.messageForUser, info, showCurrentDay);
				} catch (XmlParserException e) {
					return new RefreshResult(false, getString(R.string.message_parse_fail), info, showCurrentDay);
				}
				return new RefreshResult(true, null, info, showCurrentDay);
			}

			@Override
			protected void onPostExecute(RefreshResult result) {
				super.onPostExecute(result);

				PreferencesUtils.saveString(MainActivity.this,
					getString(R.string.prefkey_last_update),
					DateTimeUtils.formatFull(DateTimeUtils.now()));

				Intent intent = new Intent(REFRESH_BROADCAST_ACTION);
				intent.addCategory(Intent.CATEGORY_DEFAULT);
				intent.putExtra(RefreshResultListener.KEY_REFRESH_RESULT_DATA, result);
				LocalBroadcastManager.getInstance(MyApp.getInstance()).sendBroadcast(intent);

				Utils.logv(result.successful ? "data refreshed" : "data refresh failed");

				MyApp.refreshingNow = false;
			}
		}.execute();
	}



	private void saveCategories(List<Canteen> canteens) {
		String prefkey = getString(R.string.prefkey_categories_all);
		List<String> categories = PreferencesUtils.loadStringList(this, prefkey);
		for (Canteen canteen: canteens) {
			for (DailyMenu menu: canteen.dailyMenus) {
				for (MenuCategory cat: menu.categories) {
					if (Utils.containsIgnoreCase(categories, cat.name)) {
						categories.add(cat.name);
						PreferencesUtils.addNewUniqueToStringList(this, prefkey, cat.name);
					}
				}
			}
		}
	}



	private class RefreshResult implements Serializable {
		private static final long serialVersionUID = -4787227368884460639L;
		boolean successful;
		String messageToShow;
		String infoFromServer;
		boolean showCurrentDay;
		public RefreshResult(boolean successful, String messageToShow, String infoFromServer, boolean showCurrentDay) {
			this.successful = successful;
			this.messageToShow = messageToShow;
			this.infoFromServer = infoFromServer;
			this.showCurrentDay = showCurrentDay;
		}
	}



	private class RefreshResultListener extends BroadcastReceiver {
		public static final String KEY_REFRESH_RESULT_DATA = "refresh_result_data";
		@Override
		public void onReceive(Context context, Intent intent) {
			RefreshResult result = (RefreshResult)intent.getSerializableExtra(KEY_REFRESH_RESULT_DATA);
			stopRefreshAnimation();
			if (Utils.hasValue(result.infoFromServer)) {
				handleXmlInfo(result.infoFromServer);
			}
			if (result.successful) {
				updateUi(result.showCurrentDay);
			}
			if (Utils.hasValue(result.messageToShow)) {
				Toast.makeText(MainActivity.this, result.messageToShow, Toast.LENGTH_LONG).show();
			}
		}
	}



	private void handleXmlInfo(String info) {
		List<String> shownInfos = PreferencesUtils.loadStringList(this, getString(R.string.prefkey_shown_info_list));
		if (!Utils.hasValue(info) || shownInfos.contains(info)) {
			return;
		}
		PreferencesUtils.addNewUniqueToStringList(this, getString(R.string.prefkey_shown_info_list), info);
		Dialogs.showMessage(this, info);
	}



	public void startRefreshAnimation() {
		if (refreshItem != null) {
			MenuItemCompat.setActionView(refreshItem, R.layout.refresh_animation);
		}
	}

	public void stopRefreshAnimation() {
		if (refreshItem != null) {
			MenuItemCompat.setActionView(refreshItem, null);
		}
	}



	private void updateUi(boolean showCurrentDay) {

		pagerAdapter.notifyDataSetChanged();
		if (showCurrentDay) {
			setSelectedDayInFragments(DateTimeUtils.getDayToShow(DateTimeUtils.now(), DailyMenu.SHOW_DAILY_MENU_UNTIL_HOUR));
		}

		// we remove all tabs and add them again in case something changed (number of tabs or names)

		// after adding new tab, it is selected and lastTabName value is owerwritten,
		// so we need to store it temporarily
		// I spent 15 minutes debugging this stupidity
		String lastTabNameTmp = lastTabName;
		actionBar.removeAllTabs();
		int selected = 0;
		for (int i = 0; i < pagerAdapter.getCount(); i++) {
			actionBar.addTab(
				actionBar.newTab()
				.setText(pagerAdapter.getPageTitle(i))
				.setTabListener(this));
			if (pagerAdapter.getPageTitle(i).equals(lastTabNameTmp)) {
				selected = i;
			}
		}
		viewPager.setCurrentItem(selected, false);
	}


}
