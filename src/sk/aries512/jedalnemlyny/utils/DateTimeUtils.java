package sk.aries512.jedalnemlyny.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import sk.aries512.jedalnemlyny.MyApp;
import sk.aries512.jedalnemlyny.model.DailyMenu;

public class DateTimeUtils {



	/**
	 * Returns current date and time in an appropriate timezone.
	 */
	public static Calendar now() {
		return Calendar.getInstance(MyApp.getAppTimeZone());
	}



	private static final SimpleDateFormat datetimeFormatFull = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
	private static final SimpleDateFormat datetimeFormatFullUi = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
	private static final SimpleDateFormat datetimeFormatSimple = new SimpleDateFormat("yyyy-MM-dd");

	public static Calendar parseFull(String str) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(datetimeFormatFull.parse(str));
		return cal;
	}

	public static String formatFull(Calendar cal) {
		return datetimeFormatFull.format(cal.getTime());
	}

	public static String formatFullUi(Calendar cal) {
		return datetimeFormatFullUi.format(cal.getTime());
	}

	public static Calendar parseSimple(String str) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(datetimeFormatSimple.parse(str));
		return cal;
	}



	private static final String[] months = new String[]
		{ "január", "február", "marec", "apríl", "máj", "jún", "júl", "august", "september", "október", "november", "december"};

	// this... ahem... "programming language" doesn't even have initializers for collections... WHAT YEAR IS IT
	private static final Map<Integer, String> days = new HashMap<Integer, String>();
	static {
		days.put(Calendar.MONDAY, "pondelok");
		days.put(Calendar.TUESDAY, "utorok");
		days.put(Calendar.WEDNESDAY, "streda");
		days.put(Calendar.THURSDAY, "štvrtok");
		days.put(Calendar.FRIDAY, "piatok");
		days.put(Calendar.SATURDAY, "sobota");
		days.put(Calendar.SUNDAY, "nedeľa");
	}


	public static String formatDateUiRich(Calendar cal) {
		// no slovak locale? let's declare war!
		String str = days.get(cal.get(Calendar.DAY_OF_WEEK)) + ", " + cal.get(Calendar.DAY_OF_MONTH) + ". " + months[cal.get(Calendar.MONTH)];
		Calendar tmp = now();
		if (areSameDay(tmp, cal)) {
			str += " (dnes)";
		} else {
			tmp.add(Calendar.DAY_OF_MONTH, 1);
			if (areSameDay(tmp, cal)) {
				str += " (zajtra)";
			}
		}
		return str;
	}



	public static boolean areSameDay(Calendar a, Calendar b) {
		return a.get(Calendar.YEAR) == b.get(Calendar.YEAR) &&
			a.get(Calendar.DAY_OF_YEAR) == b.get(Calendar.DAY_OF_YEAR);
	}



	/**
	 * Returns day to show in UI for the current time passed in argument.
	 * Time of the day is set to noon.
	 * You should compare returned day with the day of the {@link DailyMenu}
	 * using {@link areSameDay} method.
	 */
	public static Calendar getDayToShow(Calendar now, int showCurrentDayUntilHour) {
		Calendar ret = (Calendar)now.clone();
		ret.set(Calendar.HOUR_OF_DAY, 12);
		ret.set(Calendar.MINUTE, 0);
		ret.set(Calendar.SECOND, 0);
		ret.set(Calendar.MILLISECOND, 0);
		// too late, show tommorow daily menu
		if (now.get(Calendar.HOUR_OF_DAY) > showCurrentDayUntilHour) {
			ret.add(Calendar.DAY_OF_MONTH, 1);
		}
		return ret;
	}

}
