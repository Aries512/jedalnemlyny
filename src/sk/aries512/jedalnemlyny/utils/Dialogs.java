package sk.aries512.jedalnemlyny.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Dialogs {



	public static void showMessage(Context context, String text) {
		new AlertDialog.Builder(context)
		.setMessage(text) // not using title because it has max. 2 lines only
		.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		})
		.show();
	}


}
