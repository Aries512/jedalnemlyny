package sk.aries512.jedalnemlyny.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;
import org.apache.http.util.ByteArrayBuffer;

public class FileUtils {



	// based on http://www.helloandroid.com/tutorials/how-download-fileimage-url-your-device
	public static void downloadFile(URL url, File destination) throws IOException {

		URLConnection ucon = url.openConnection();
		InputStream is = ucon.getInputStream();
		ByteArrayBuffer baf = new ByteArrayBuffer(1<<13);
		BufferedInputStream bis = new BufferedInputStream(is);

		int current = 0;
		try {
			while ((current = bis.read()) != -1) {
				baf.append((byte)current);
			}
		} finally {
			bis.close();
		}

		FileOutputStream fos = new FileOutputStream(destination);
		fos.write(baf.toByteArray());
		fos.close();

	}



	public static String getFileContent(File file) throws IOException, FileNotFoundException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append('\n');
			}
		} finally {
			if (br != null) {
				br.close();
			}
		}
		return sb.toString();
	}



	public static void decompressGzip(File archive, File decompressed) throws FileNotFoundException, IOException {
		GZIPInputStream gis = null;
		FileOutputStream out = null;
		try {
			gis = new GZIPInputStream(new BufferedInputStream(new FileInputStream(archive)));
			out = new FileOutputStream(decompressed);
			final byte[] buffer = new byte[1<<16];
			int n = 0;
			while (-1 != (n = gis.read(buffer))) {
				out.write(buffer, 0, n);
			}
		} finally {
			if (out != null) {
				out.close();
			}
			if (gis != null) {
				gis.close();
			}
		}
	}

}
