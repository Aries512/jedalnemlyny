package sk.aries512.jedalnemlyny.utils;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesUtils {

	public static void saveString(Context context, String key, String value) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String loadString(Context context, String key, String defValue) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getString(key, defValue);
	}



	public static void saveInt(Context context, String key, int value) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static int loadInt(Context context, String key, int defValue) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getInt(key, defValue);
	}



	public static void saveStringList(Context context, String key, String... values) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sp.edit();
		JSONArray arr = new JSONArray();
		for (String value: values) {
			arr.put(value);
		}
		editor.putString(key, arr.toString());
		editor.commit();
	}

	public static List<String> loadStringList(Context context, String key) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		List<String> strs = new ArrayList<String>();
		JSONArray json;
		try {
			json = new JSONArray(sp.getString(key, ""));
		} catch (JSONException e) {
			return strs;
		}
		for (int i = 0; i < json.length(); ++i) {
			try {
				strs.add(json. getString(i));
			} catch (JSONException e) { }
		}
		return strs;
	}

	public static void addNewUniqueToStringList(Context context, String key, String... values) {
		List<String> current = loadStringList(context, key);
		for (String value: values) {
			if (!current.contains(value)) {
				current.add(value);
			}
		}
		saveStringList(context, key, current.toArray(new String[current.size()]));
	}

	public static void removeFromStringList(Context context, String key, String... values) {
		List<String> current = loadStringList(context, key);
		for (String value: values) {
			while (current.contains(value)) {
				current.remove(value);
			}
		}
		saveStringList(context, key, current.toArray(new String[current.size()]));
	}



}
