package sk.aries512.jedalnemlyny.utils;

import java.util.List;
import sk.aries512.jedalnemlyny.MyApp;

public class Utils {



	public static void dbg(Object... o) {
		android.util.Log.d(MyApp.LOG_TAG, prepareDbgString(o));
	}

	public static void logi(Object... o) {
		android.util.Log.i(MyApp.LOG_TAG, prepareDbgString(o));
	}

	public static void logv(Object... o) {
		android.util.Log.v(MyApp.LOG_TAG, prepareDbgString(o));
	}

	private static String prepareDbgString(Object... o) {
		if (o == null || o.length == 0) {
			android.util.Log.d(MyApp.LOG_TAG, "");
		}
		boolean first = true;
		StringBuilder sb = new StringBuilder();
		for (Object obj: o) {
			if (!first) {
				sb.append(", ");
			} else {
				first = false;
			}
			sb.append(obj == null ? "<null>" : obj.toString());
		}
		return sb.toString();
	}



	public static boolean hasValue(String str) {
		return str != null && !str.equals("");
	}

	public static boolean allHaveValue(String... strs) {
		for (String str: strs) {
			if (!hasValue(str)) {
				return false;
			}
		}
		return true;
	}



	public static boolean containsIgnoreCase(List<String> list, String elem) {
		if (list == null) {
			return false;
		}
		if (elem == null) {
			return list.contains(null);
		}
		for (String str: list) {
			if (str == null) {
				continue;
			} else {
				if (str.toLowerCase().equals(elem.toLowerCase())) {
					return true;
				}
			}
		}
		return false;
	}



}
